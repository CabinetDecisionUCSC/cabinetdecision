<! Target : Successfull Logout>
<! Auther : Bandula Kularatne >
<?php
   session_start();
   
   if(session_destroy()) {
      header("Location: index.html");
   }
?>