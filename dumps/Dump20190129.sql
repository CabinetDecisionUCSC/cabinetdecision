CREATE DATABASE  IF NOT EXISTS `dbo` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dbo`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: dbo
-- ------------------------------------------------------
-- Server version	5.7.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `commonministrylist`
--

DROP TABLE IF EXISTS `commonministrylist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commonministrylist` (
  `CommonMinistryNo` char(3) NOT NULL,
  `IndNo` int(11) NOT NULL,
  `Remarks` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commonministrylist`
--

LOCK TABLES `commonministrylist` WRITE;
/*!40000 ALTER TABLE `commonministrylist` DISABLE KEYS */;
/*!40000 ALTER TABLE `commonministrylist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpnumberlist`
--

DROP TABLE IF EXISTS `cpnumberlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpnumberlist` (
  `SNo` bigint(20) NOT NULL,
  `ItemNo` char(10) DEFAULT NULL,
  `CPNumber` char(30) NOT NULL,
  `AllMinistryStatus` tinyint(1) DEFAULT NULL,
  `CommanministryStatus` tinyint(1) DEFAULT NULL,
  `SortKey` char(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpnumberlist`
--

LOCK TABLES `cpnumberlist` WRITE;
/*!40000 ALTER TABLE `cpnumberlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpnumberlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `decisionlist`
--

DROP TABLE IF EXISTS `decisionlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decisionlist` (
  `CPSNo` bigint(20) NOT NULL,
  `MinistryNo` char(3) NOT NULL,
  `SNo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `decisionlist`
--

LOCK TABLES `decisionlist` WRITE;
/*!40000 ALTER TABLE `decisionlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `decisionlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ministrylist`
--

DROP TABLE IF EXISTS `ministrylist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ministrylist` (
  `MinistryNo` char(3) NOT NULL,
  `Ministry` char(200) DEFAULT NULL,
  `Officer` char(50) DEFAULT NULL,
  `AllStatus` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`MinistryNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ministrylist`
--

LOCK TABLES `ministrylist` WRITE;
/*!40000 ALTER TABLE `ministrylist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ministrylist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sysdiagrams`
--

DROP TABLE IF EXISTS `sysdiagrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sysdiagrams` (
  `name` varchar(160) NOT NULL,
  `principal_id` int(11) NOT NULL,
  `diagram_id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) DEFAULT NULL,
  `definition` longblob,
  PRIMARY KEY (`diagram_id`),
  UNIQUE KEY `UK_principal_name` (`principal_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sysdiagrams`
--

LOCK TABLES `sysdiagrams` WRITE;
/*!40000 ALTER TABLE `sysdiagrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `sysdiagrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tempcpnumbers`
--

DROP TABLE IF EXISTS `tempcpnumbers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tempcpnumbers` (
  `TxnNo` int(11) NOT NULL,
  `ItemNo` char(15) DEFAULT NULL,
  `CPNo` char(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tempcpnumbers`
--

LOCK TABLES `tempcpnumbers` WRITE;
/*!40000 ALTER TABLE `tempcpnumbers` DISABLE KEYS */;
/*!40000 ALTER TABLE `tempcpnumbers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmpdetail`
--

DROP TABLE IF EXISTS `tmpdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmpdetail` (
  `CPIndex` bigint(20) NOT NULL,
  `MinNo` char(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmpdetail`
--

LOCK TABLES `tmpdetail` WRITE;
/*!40000 ALTER TABLE `tmpdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmpdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'dbo'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-29 15:21:26
